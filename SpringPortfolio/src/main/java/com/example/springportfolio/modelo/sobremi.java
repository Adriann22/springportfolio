package com.example.springportfolio.modelo;

// Importando las clases necesarias de JPA (Java Persistence API)
import jakarta.persistence.*;

// Esta clase representa la entidad "sobremi" en la base de datos
@Entity
@Table(name = "sobremi")
public class sobremi {
    // Define el identificador único de cada entrada de "sobremi" en la base de datos
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // Atributo que representa el texto relacionado con la descripción personal
    private String texto_sobremi;

    // Métodos para acceder y modificar el identificador
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    // Métodos para acceder y modificar el texto relacionado con la descripción personal
    public String getTexto_sobremi() {
        return texto_sobremi;
    }

    public void setTexto_sobremi(String texto_sobremi) {
        this.texto_sobremi = texto_sobremi;
    }
}
