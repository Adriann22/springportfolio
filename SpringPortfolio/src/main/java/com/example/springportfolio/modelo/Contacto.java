package com.example.springportfolio.modelo;

// Importando las clases necesarias de JPA (Java Persistence API)
import jakarta.persistence.*;

// Esta clase representa la entidad "contacto" en la base de datos
@Entity
@Table(name = "contactos")
public class Contacto {
    // Define el identificador único de cada contacto en la base de datos
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // Atributos que representan el nombre, el correo electrónico y el mensaje del contacto
    private String nombre;
    private String email;
    private String mensaje;

    // Métodos para acceder y modificar el identificador
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    // Métodos para acceder y modificar el nombre del contacto
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    // Métodos para acceder y modificar el correo electrónico del contacto
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    // Métodos para acceder y modificar el mensaje del contacto
    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
}
