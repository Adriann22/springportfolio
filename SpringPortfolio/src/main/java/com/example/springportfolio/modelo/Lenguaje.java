package com.example.springportfolio.modelo;

// Importando las clases necesarias de JPA (Java Persistence API)
import jakarta.persistence.*;

// Esta clase representa la entidad "lenguaje" en la base de datos
@Entity
@Table(name = "lenguajes")
public class Lenguaje {
    // Define el identificador único de cada lenguaje en la base de datos
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // Atributos que representan el nombre y la imagen del lenguaje
    private String nombre;
    private String img;

    // Métodos para acceder y modificar el identificador
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    // Métodos para acceder y modificar el nombre del lenguaje
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    // Métodos para acceder y modificar la imagen del lenguaje
    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }
}
