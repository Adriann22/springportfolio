package com.example.springportfolio.modelo;

// Importando las clases necesarias de JPA (Java Persistence API)
import jakarta.persistence.*;

// Esta clase representa la entidad "proyecto" en la base de datos
@Entity
@Table(name = "proyectos")
public class Proyecto {
    // Define el identificador único de cada proyecto en la base de datos
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // Atributo que representa la imagen del proyecto
    private String img;

    // Métodos para acceder y modificar el identificador
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    // Métodos para acceder y modificar la imagen del proyecto
    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }
}
