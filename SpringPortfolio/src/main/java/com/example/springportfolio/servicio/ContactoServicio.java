package com.example.springportfolio.servicio;

// Importando las clases necesarias
import com.example.springportfolio.modelo.Contacto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.springportfolio.repositorio.ContactoRepositorio;
import java.util.List;

// Esta clase de servicio maneja la lógica relacionada con los contactos
@Service
public class ContactoServicio {

    // Inyecta el repositorio de contactos
    @Autowired
    private ContactoRepositorio contactoRepositorio;

    // Método para obtener todos los contactos
    public List<Contacto> getAllContactos() {
        return contactoRepositorio.findAll();
    }
}
