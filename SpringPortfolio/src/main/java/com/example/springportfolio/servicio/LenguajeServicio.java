package com.example.springportfolio.servicio;

import com.example.springportfolio.modelo.Lenguaje;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.springportfolio.repositorio.LenguajeRepositorio;
import java.util.List;

@Service
public class LenguajeServicio {

    @Autowired
    private LenguajeRepositorio lenguajeRepositorio;

    public List<Lenguaje> getAllLenguajes() {
        return lenguajeRepositorio.findAll();
    }
}
