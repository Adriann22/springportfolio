package com.example.springportfolio.servicio;

import com.example.springportfolio.modelo.sobremi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.springportfolio.repositorio.sobremiRepositorio;
import java.util.List;

@Service
public class sobremiServicio {

    @Autowired
    private sobremiRepositorio sobremiRepositorio;

    public List<sobremi> getAllsobremi() {
        return sobremiRepositorio.findAll();
    }
}
