package com.example.springportfolio.servicio;

import com.example.springportfolio.modelo.Proyecto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.springportfolio.repositorio.ProyectoRepositorio;
import java.util.List;

@Service
public class ProyectoServicio {

    @Autowired
    private ProyectoRepositorio proyectoRepositorio;

    public List<Proyecto> getAllProyectos() {
        return proyectoRepositorio.findAll();
    }
}
