package com.example.springportfolio.repositorio;

import com.example.springportfolio.modelo.sobremi;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface sobremiRepositorio extends JpaRepository<sobremi, Long> {
}