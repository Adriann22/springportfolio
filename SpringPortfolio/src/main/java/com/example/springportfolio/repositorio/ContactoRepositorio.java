package com.example.springportfolio.repositorio;

// Importando las clases necesarias
import com.example.springportfolio.modelo.Contacto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

// Esta interfaz actúa como un repositorio para la entidad "Contacto"
@Repository
public interface ContactoRepositorio extends JpaRepository<Contacto, Long> {
}
