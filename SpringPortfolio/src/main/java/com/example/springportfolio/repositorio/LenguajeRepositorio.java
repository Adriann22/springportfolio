package com.example.springportfolio.repositorio;

import com.example.springportfolio.modelo.Lenguaje;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LenguajeRepositorio extends JpaRepository<Lenguaje, Long> {
}
