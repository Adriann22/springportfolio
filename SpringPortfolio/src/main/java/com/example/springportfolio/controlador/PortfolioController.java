package com.example.springportfolio.controlador;

// Importando las clases necesarias
import com.example.springportfolio.servicio.sobremiServicio;
import com.example.springportfolio.servicio.ContactoServicio;
import com.example.springportfolio.servicio.LenguajeServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import com.example.springportfolio.servicio.ProyectoServicio;

// Esta clase es un controlador para manejar las páginas web del portafolio
@Controller
public class PortfolioController {

    // Inyecta los servicios necesarios

    @Autowired
    private sobremiServicio sobremiServicio; // Servicio para manejar la descripción personal

    @Autowired
    private LenguajeServicio lenguajeServicio; // Servicio para manejar los lenguajes de programación

    @Autowired
    private ProyectoServicio proyectoServicio; // Servicio para manejar los proyectos

    @Autowired
    private ContactoServicio contactoServicio; // Servicio para manejar los contactos

    // Método para manejar las solicitudes GET a la raíz del sitio
    @GetMapping("/")
    public String Paginaweb(Model model) {
        // Agrega información al modelo para pasar a la vista

        // Añade los lenguajes de programación al modelo
        model.addAttribute("lenguajes", lenguajeServicio.getAllLenguajes());
        // Añade los proyectos al modelo
        model.addAttribute("proyectos", proyectoServicio.getAllProyectos());
        // Añade los contactos al modelo
        model.addAttribute("contactos", contactoServicio.getAllContactos());
        // Añade la descripción personal al modelo
        model.addAttribute("sobremi", sobremiServicio.getAllsobremi());

        // Devuelve el nombre de la vista que debe renderizarse
        return "Paginaweb";
    }
}
