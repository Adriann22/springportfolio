DROP DATABASE SpringPortfolioSQL;
CREATE DATABASE SpringPortfolioSQL;
USE SpringPortfolioSQL;

CREATE TABLE sobremi (
    id INT auto_increment primary key,
    texto_sobremi TEXT
);
 
CREATE TABLE lenguajes (
    id int auto_increment primary key,
	nombre VARCHAR(100),
    img VARCHAR(100)
);

CREATE TABLE proyectos (
	id int auto_increment primary key,
	img VARCHAR(100)
);
 
CREATE TABLE contactos (
    id INT auto_increment primary key,
    nombre VARCHAR(100),
	email VARCHAR(100),
    mensaje text
);

INSERT INTO proyectos (img) VALUES
    ('./loginteams.png'),
    ('./trabajo2.png'),
    ('./trabajo3.png'),
    ('./trabajo4.png');
INSERT INTO lenguajes (nombre, img) VALUES
	('Python', './python.png'),
	('Java', './java.png'),
	('JavaScript', './javascript.png'),
	('HTML', './html.png'),
	('CSS', './css.png'),
	('C++', './cmas.png');
    
INSERT INTO sobremi (texto_sobremi) VALUES
	('¡Hola! Soy Adrian Solomakha, un entusiasta de la informática con una pasión innata por la tecnología y la resolución de problemas. Desde una edad temprana, mi fascinación por el mundo digital me llevó a explorar diversos aspectos de la informática, desde la programación hasta la administración de sistemas.

Mi experiencia abarca áreas como el desarrollo de software, donde he trabajado en proyectos que van desde aplicaciones web hasta soluciones personalizadas. Poseo habilidades sólidas en lenguajes de programación como Java, Python, CSS, JavaScript, HTML, C++ y siempre estoy buscando oportunidades para expandir mis conocimientos en nuevas tecnologías emergentes.

Además, tengo experiencia en la administración de sistemas, donde he trabajado en entornos complejos para garantizar la eficiencia y la seguridad de los sistemas informáticos. Mi enfoque proactivo para abordar desafíos y mi capacidad para aprender rápidamente me han permitido destacar en ambientes dinámicos y exigentes.

Soy un defensor entusiasta del aprendizaje continuo y participo regularmente en comunidades en línea, conferencias y eventos relacionados con la informática. Creo firmemente en la importancia de mantenerse actualizado en un campo tan dinámico como la tecnología.

Además de mis habilidades técnicas, me considero un comunicador efectivo y un colaborador comprometido. Disfruto trabajando en equipo, aportando ideas innovadoras y ayudando a construir soluciones que marquen la diferencia.

En resumen, soy un profesional de la informática apasionado y orientado a resultados, siempre listo para enfrentar nuevos desafíos y contribuir al emocionante mundo de la tecnología.

¡Espero con interés la oportunidad de colaborar contigo y llevar nuestras ideas a nuevos horizontes digitales!');
    
    
    
SELECT * FROM SpringPortfolioSQL.lenguajes;
SELECT * FROM SpringPortfolioSQL.proyectos;
SELECT * FROM SpringPortfolioSQL.sobremi;



